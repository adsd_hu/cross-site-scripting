# Cross-Site Scripting (XSS) Exercise

Use _Cross-Site Scripting_ (XSS) to exploit vulnerabilities in this page.

1. Show alert on page displaying 'you are hacked!'
2. Show alert on page displaying access token from cookie
3. Fix the page by sanitizing user input (escape [<>])
4. Fix the page by not setting innerHTML
